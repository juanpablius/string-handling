var clientes_csv =
  'ID,Nombre,Email,Facturado,Localidad\n' +
  '1,Juan Martinez,juan@mail.com,15000,Valladolid\n' +
  '2,Francisco Gomez,paco@my-mail.com,20000,Salamanca\n' +
  '3,Gerardo Puente,gerardo-puente@mail-sample.com, ,Barcelona\n' +
  '4,Javier Cruz,jcruz@mail.com,12400.27,Madrid\n' +
  '5,Fernando Martin, fm@mail.com,53000,Soria\n';

const NUMERIC_COLUMNS = ['ID', 'Facturado'];
const SENSIBLE_COLUMNS = ['Nombre', 'Email', 'Facturado'];
const SUBSTITUTION_CHARACTER = 'X';
const alphaNumericRegex = /[a-zA-Z]/g;
const numericRegex = /[-]{0,1}[\d]*[.]{0,1}[\d]+/g;

const average = (array) =>
  array.reduce((prev, current) => prev + current, 0) / array.length;
const sorter = (array) => array.sort();

const clientsLineArray = clientes_csv.split('\n');
const titles = clientsLineArray[0].split(',');

const clientsInfo = clientsLineArray.slice(1, clientsLineArray.length - 1);

const numbers = clientsInfo.map((row) => {
  const splitedRow = row.split(',');
  return Number(splitedRow[0]);
});

const numbersAverage = average(numbers);

const strings = clientsInfo.map((row) => {
  const splitedRow = row.split(',');
  return String(splitedRow.slice(1, splitedRow.length));
});

const clientes_masked_csv = clientsInfo.map((row) => {
  let editedString = row.replace(alphaNumericRegex, SUBSTITUTION_CHARACTER);
  editedString = editedString.toString().replace(numericRegex, numbersAverage);
  return editedString;
});

console.log(clientes_masked_csv);
